#pragma once

class Point2D {
	double x;
	double y;
	float rho;
	float theta;
public:
	Point2D();
	Point2D(float, float);

	void setX(float);
	void setY(float);

	float getX();
	float getY();

	float distance(Point2D);

	float distance1();

	void rotatePoint(float, float, double);

	void translatePoint(float, float, float, float);

		void display();
};
	

