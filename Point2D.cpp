#pragma once
#include<iostream>
#include <cmath>
#include"Point2D.h"

using namespace std;

Point2D::Point2D()
{
	cout << "Dziala konstruktor klasy Point2D bez arg.\n";
	this->x = 0.0f; //T1
	this->y = 0.0f;
}

Point2D::Point2D(float x, float y)
{
	cout << "Dziala konstruktor klasy Point2D z arg.\n";
	this->x = x;

	if (y >= 0.0f) {
		this->y = y;
	}
	else {
		do {
			cout << "Y nie moze byc mniejszy od 0.0\n";
			cout << "Podaj prawidlowa wartosc\n";
			cin >> y;
		} while (y < 0.0f);
		this->y = y;
	}
		
}

void Point2D::setX(float x) {
	//x = _x;
	this->x = x;
}

void Point2D::setY(float y) {
	//y = _y;
	this->y = y;
}

float Point2D::getX()
{
	return this->x;
}

float Point2D::getY()
{
	return this->y;
}

float Point2D::distance(Point2D p)
{
	return sqrtf((this->x - p.x) * (this->x - p.x) + powf(this->y - p.y, 2));
}

float Point2D::distance1()
{
	return sqrtf((this->x - 0) * (this->x - 0) + powf(this->y - 0, 2));
}

void Point2D::rotatePoint(float tmpX, float tmpY, double radiany) 
{
	double x_1 = this->x * cos(radiany) - this->y * sin(radiany);
	double y_1 = this->x * sin(radiany) - this->y * cos(radiany);

	cout << "Wspolrzedne punktu (" << tmpX << ", " << tmpY << ") po obrocie o kat " << radiany << " to " << "(" << x_1 << ", " << y_1 << ")" << endl;
}

void Point2D::translatePoint(float tmpX, float tmpY, float a, float b)
{
	float x_2 = this->x + a;
	float y_2 = this->x + b;

	cout << "Wspolrzedne punktu (" << tmpX << ", " << tmpY << ") w przesunieciu o wektor [" << a << "," << b << "] sa rowne " << "(" << x_2 << ", " << y_2 << ")" << endl;
}

void Point2D::display()
{
	cout << "Punkt (" << this->x << ", " << this->y << ")\n";
}


