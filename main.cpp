#include<iostream>
#include <cmath>
#include"Point2D.h"

using namespace std;


int main() {


	//std::cout << "Hello C++\n"; bez using namespace std
	//cout << "Hello C++\n";

	Point2D my_point;

	cout << "Rozmiar obiektu my_point " <<
		sizeof(my_point) << endl;

	// my_point.x = 4.67; ZA TO JEST ZERO NA SPRAWDZIANIE!!! T2

	my_point.setX(4.88); // T3
	//my_point.setY(5.9); //moje dopisanie/sprawdzenie działania programu -> jest OK

	cout << "Wsp. X " << my_point.getX() << endl;
	cout << "Wsp. Y " << my_point.getY() << endl;

	cout << "\n---------------------------\n";

	float tmpX, tmpY;
	cout << "Podaj x=";
	cin >> tmpX;
	cout << "Podaj y=";
	cin >> tmpY;

	Point2D new_point(tmpX, tmpY);
	new_point.display();
	my_point.display(); //moje dopisanie/sprawdzenie działania programu ->jest OK

	cout << "\n---------------------------\n";

	cout << "Odleglosc " << my_point.distance(new_point) << endl;

	cout << "Odleglosc " << new_point.distance1() << endl;

	float rho = new_point.distance1();

	cout << "Odleglosc " << rho << endl;

	cout << "\n---------------------------\n";

	double theta = asin(tmpY/rho);

	cout << "Kat z osia OX ma miare: " << theta << " radianow" << endl;

	double stopnie1 = (theta * 180.0f) / 3.14f;

	cout << "Kat " << theta << " w stopniach ma miare rowna " << stopnie1 << endl;

	cout << "\n---------------------------\n";

	double stopnie;
	cout << "Podaj kat w stopniach: ";
	cin >> stopnie;
	
	double radiany = (stopnie * 3.140f) / 180.0;

	cout << "Kat " << stopnie << " ma miare rowna " << radiany << " radianow" << endl;

	cout << "\n---------------------------\n";

	new_point.rotatePoint(tmpX, tmpY, radiany);

	cout << "\n---------------------------\n";

	float a, b;
	cout << "Podaj a=";
	cin >> a;
	cout << "Podaj b=";
	cin >> b;

	new_point.translatePoint(tmpX, tmpY, a, b);

	return 0;
}

